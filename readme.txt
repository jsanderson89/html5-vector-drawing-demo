HTML5 Vector Drawing Demo


This demo is an attempt to create a GUI based vector drawing program in
HTML5. 

Individual points are drawn by clicking within the canvas. They can also
be generated at specific points by entering the data into the form beneath
the canvas and pressing the "Submit" button.


Issues:
	-Currently, there is no method for drawing an independent shape.
	 In the future, once an existing point is clicked, the next line 
	 will automatically be drawn to that point. That shape will then
	 be added to a "shape" array to manage display objects. The next 
	 point will not connect to the previously drawn shape.

	-There is currently no support for bezier curves.

	-Points are currently immobile once placed, and cannot be
	 deleted. In the future, points and shapes should be freely
	 editable.

	-Once Shapes are implemented, it is my intent to allow the 
	 underlying Canvas code to be echoed back to the user, allowing
	 users to use the program to quickly generate HTML drawing code
	 in a WYSIWYG editor.
	 
This project uses jQuery v1.9.1, which is included.
Copyright information for jQuery can be found in the file header.


All other work Copyright Jesse Anderson, 2013,2014.