function pointDrawer(canvasId)
{
	var canvas = document.getElementById(canvasId);
	var context = document.getElementById(canvasId).getContext('2d');
	var point_radius = 5;
	var offset_circle_to_cursor = 10;
	
	var points = [];
	var pointPairs = [];
	
	var currentPointSet = [];
	var currentHighlightedPoint;

	var currentShapeSet = [];
	//Store All The Shapes
	var listOfShapes = [];
	
	
	//DEV_doThis();

	canvas.onclick = MouseClick;
	canvas.onmousemove = MouseMove;
	//canvas.onmousedown = StartDrag;
	//canvas.onmousemove = WhileDragging;
	//canvas.onmouseup = StopEnd;
	//canvas.onmouseout = StopEnd;

	function Point()
	{
		var X;
		var Y;
	}
	
	function PointPair()
	{
		var point1;
		var point2;
	}
	
	function DEV_doThis()
	{
		var p = new Point();
		p.X = 50;
		p.Y = 50;
		
		var p2 = new Point();
		p2.X = 200;
		p2.Y = 200;
		
		var pp = new PointPair();
		pp.point1 = p;
		pp.point2 = p2;
		
		var listOfPoints = [];
		
		listOfPoints.push(pp);
		
		
		clear();
		var i;
		for(i = 0; i<listOfPoints.length; i++)
		{
			drawCircleGraphic(Number(listOfPoints[i].point1.X), Number(listOfPoints[i].point1.Y));
			drawCircleGraphic(Number(listOfPoints[i].point2.X), Number(listOfPoints[i].point2.Y));
		
			context.beginPath();
			context.moveTo(Number(listOfPoints[i].point1.X),Number(listOfPoints[i].point1.Y));
			context.lineTo(Number(listOfPoints[i].point2.X),Number(listOfPoints[i].point2.Y));
			context.stroke();
		}
	}


	function MouseClick(e)
	{
		var mouseX = 0;
		var mouseY = 0;

		
		if(!e)
		{
			e = window.event;
		}

		if(e.pageX || e.pageY)
		{
			mouseX = e.pageX;
			mouseY = e.pageY;
		}

		else if (e.clientX || e.clientY)
		{
			mouseX = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
			mouseY = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
		}

		mouseX -= canvas.offsetLeft +=offset_circle_to_cursor;
		mouseY -= canvas.offsetTop +=offset_circle_to_cursor;
		
		
		if(!currentHighlightedPoint)
		{
			createCircle(mouseX,mouseY);
		}
		
		else
		{
			createCircle(currentHighlightedPoint.X,currentHighlightedPoint.Y)
			
			//this should seal the shape
			var isPointPresent;

			for(var i=0; i<=currentPointSet.length-1;i++)
			{
				console.log("entered");
				if(i.X === currentHighlightedPoint.X && i.Y === currentHighlightedPoint.Y)
				{
					console.log("is true");
					isPointPresent = true;
				}
			}

			if(isPointPresent)
			{
				listOfShapes.push(currentPointSet);
				currentPointSet.length = 0;
			}
		}
		

		
	}
	
	function MouseMove(e)
	{
		//console.log("MouseOver");
		var mouseX = 0;
		var mouseY = 0;
		
		currentHighlightedPoint = null;
		
		
		if(!e)
		{
			e = window.event;
		}

		if(e.pageX || e.pageY)
		{
			mouseX = e.pageX;
			mouseY = e.pageY;
		}

		else if (e.clientX || e.clientY)
		{
			mouseX = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
			mouseY = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
		}

		mouseX -= canvas.offsetLeft +=offset_circle_to_cursor;
		mouseY -= canvas.offsetTop +=offset_circle_to_cursor;
		
		var i;
		for(i = 0; i < points.length; i++)
		{
			//console.log("for")
			if ((mouseX-points[i].X)*(mouseX-points[i].X)+(mouseY-points[i].Y)*(mouseY-points[i].Y) < (point_radius*point_radius))
			{
				var highlightedPoint = new Point();
				highlightedPoint.X = points[i].X;
				highlightedPoint.Y = points[i].Y;
				
				//console.log(highlightedPoint);
				
				currentHighlightedPoint = highlightedPoint;
				//console.log(currentHighlightedPoint);
			}
			else
			{
				//console.log("false");
			}
		}

		draw();
	}

	/*
	function MousePosition(e)
	{
		event = (event ? event : window.event);
		return
		{
			x: event.pageX - canvas.offsetLeft +=offset_circle_to_cursor;
			y: event.pageY - canvas.offsetTop +=offset_circle_to_cursor;
		}
	}
	*/

	function createCircle(x,y)
	{
		var p = new Point();
		p.X = x;
		p.Y = y;
		points.push(p);
		currentPointSet.push(p);
		
		if(currentPointSet.length === 2)
		{
			createLineSegment();
		}
		
		draw();
	}
	
	function createLineSegment()
	{
		var pp = new PointPair();
		pp.point1 = currentPointSet[0];
		pp.point2 = currentPointSet[1];
		pointPairs.push(pp);
		
		var storeValue = currentPointSet[1];
		currentPointSet.length = 0;
		currentPointSet[0] = storeValue;
		
		//console.log("line seg created: " + "'(" + pp.point1.X + "," + pp.point1.Y + ")'"+ "'(" + pp.point2.X + "," + pp.point2.Y + ")'");
	}

	function drawCircleGraphic(x,y)
	{
		//context.fillRect(25,25,100,100);
  		//context.clearRect(45,45,60,60);
  		//context.strokeRect(50,50,50,50);
  		context.fillStyle = "#000000";
  		context.beginPath();
  		context.arc(x,y, point_radius,0, Math.PI*2, true);
  		context.closePath();
  		context.fill();
	}
	
	function drawHighlightGraphic(x,y)
	{
  		context.fillStyle = "#FF0000";
  		context.beginPath();
  		context.arc(x,y, point_radius,0, Math.PI*2, true);
  		context.closePath();
  		context.fill();
	}
	
	
	function drawLineSegment(xOne,yOne,xTwo,yTwo)
	{
		context.beginPath();
		context.moveTo(Number(xOne),Number(yOne));
		context.lineTo(Number(xTwo),Number(yTwo));
		context.stroke();
	}

	function draw()
	{
		clear();
		var i;
		for(i = 0; i < points.length; i++)
		{
			drawCircleGraphic(Number(points[i].X), Number(points[i].Y));
		}
		
		//console.log (pointPairs.length);
		
		var j;
		if (pointPairs.length > 0)
		{
			for(j = 0; j <= pointPairs.length-1; j++)
			{
				drawLineSegment(Number(pointPairs[j].point1.X), Number(pointPairs[j].point1.Y), Number(pointPairs[j].point2.X), Number(pointPairs[j].point2.Y));
			}
		}
		
		if(currentHighlightedPoint)
		{
			//console.log("draw");
			drawHighlightGraphic(currentHighlightedPoint.X, currentHighlightedPoint.Y);
		}
	}

	function clear()
	{
		context.clearRect(0,0,canvas.width, canvas.height)
	}


}